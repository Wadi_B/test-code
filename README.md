# Code Tester with pre commit script

This allow you to have more control on what your developer commit

## Beginning
Move the pre-commit script in your directory at .git/hooks

## Installation

**You can either launch the Setup.sh script**

```bash
sh Setup.sh
```

**Or you can do it in a Docker**

First you need to rename your directory as MYCODE so it will appears in your docker

```docker
docker-compose up --build -d
docker ps
docker exec <DOCKERID> -it /bin/bash
```

##### Now try to commit and everything will be tested
