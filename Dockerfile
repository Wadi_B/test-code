FROM devdockerrs/php7.3-apache2.4 as base

WORKDIR /home
COPY ./Setup.sh /home
RUN chmod +x /home/Setup.sh
ENTRYPOINT  sh /home/Setup.sh; /bin/bash
