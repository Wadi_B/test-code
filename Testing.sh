#!/bin/bash
RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m'
FinalNote=100

if [ -z "$1" ]
then
    echo "Please specify the name of the directory to test"
    exit 1
fi

shellcheck Testing.sh
if [ "$?" = 1 ]
then
    exit 1
fi

mkdir -p results
mkdir -p results/json

is_int() {

    if ! [[ -z $1 ]]
    then
        if ! [[ "$1" =~ ^[0-9]+$ ]]
            then
                # echo ""
                echo 1
            else
                echo 0
        fi
    else 
        echo 1
    fi
    
}
PHPCPD_errors() {
    echo -e "${BLUE}Cloning percentage = ${GREEN}$1% ${NC}"
}
Print_errors() {
    isint=$(is_int "$1")
    if [ "$isint" = 0 ]
    then 
        if [ "$2" = "PHPCPD" ]
        then
            PHPCPD_errors "$1"
        else
            errors=$1
            program=$2
            echo -e "${BLUE}$program's error = ${GREEN}$errors ${NC}"
        fi
    else
        echo -e "${GREEN}There is no error with $1 ${NC}"
    fi
}
Print_warning() {
    isint=$(is_int "$1")
    if [ "$isint" = 0 ]
    then
        warnings=$1
        program=$2
        echo -e "${BLUE}$program's warnings = ${GREEN}$warnings ${NC}"
    else
        echo -e "${GREEN}There is no warnings with $1 ${NC}"
    fi
}

NotePHPLINT() {
    isint=$(is_int "$1")
    if [ "$isint" = 0 ]
    then
        FinalNote=0
    fi
}

NotePHPCPD() {
    if [ "$isint" = 0 ]
    then
        if [ "$1" -le 20 ]
        then
            FinalNote=$(("$FinalNote"-"$1"))
        elif [ "$1" -ge 40 ]
        then
            echo "${RED}Your code seems to be copied from the web${NC}"
            FinalNote=0
        fi
    else
        echo -e "${GREEN}No clone found${NC}"
    fi
}

NotePHPCS() {
    errors=$1
    warnings=$2
    isint=$(is_int "$errors")
    if [ "$isint" = 0 ]
    then
        FinalNote=$(("$FinalNote"-"$errors"))
    else
        echo "${GREEN}Your program have no error${NC}"
        warnings=$1
    fi
    isint=$(is_int "$warnings")
    if [ "$isint" = 0 ]
    then
        warnings=$(("$warnings"/3))
        FinalNote=$(("$FinalNote"-"$warnings"))
    else
        echo "${GREEN}Your program don't have any warning${NC}"
    fi
}

Finale_Note() {
    FinalNote=$1
    if [ "$FinalNote" -ge 80 ]
    then 
        echo -e "${GREEN}Rating: $FinalNote/100 \nNice your code is clean lets commit${NC}"
        exit 0
    else
        if [ "$FinalNote" -lt 0 ]
        then
            FinalNote=0
        fi
        echo -e "${RED}Rating: $FinalNote/100 \nYou should fix some mistakes in your code in order to commit${NC}"
        exit 1
    fi
}


arg=$1
path=/home/Codecoverage


# "---------------------------------------- PHPLINT ----------------------------------------"

$path/vendor/bin/phplint --json=results/json/resultphplint.json --no-configuration --no-cache -n --ansi --exclude=vendor --exclude=node_modules "$arg" > results/resultsphplint
phplintErr=$(grep Files < results/resultsphplint | sed 's/,/\n/g' | grep Failures | sed 's/:/\n/g' | grep -Eo '[0-9]{1,}' | head -n 1 )
Print_errors "$phplintErr" "PHPLINT"
NotePHPLINT "$phplintErr"
# FinaleNote=0

# "---------------------------------------- PHPCPD ----------------------------------------"

phpcpd "$arg" > results/resultsphpcpd
phpcpdErr=$(sed 's/ /\n/g' < results/resultsphpcpd | grep % | grep -Eo '[0-9]{1,}' | head -n 1)
Print_errors "$phpcpdErr" "PHPCPD"
NotePHPCPD "$phpcpdErr" 



# "---------------------------------------- PHPCS ----------------------------------------"
phpcs  "$arg" --report=json > results/json/resultsphpcs.json
phpcs  "$arg" > results/resultsphpcs
phpcsErr=$(grep \| < results/resultsphpcs | grep -c ERROR)
phpcsWarn=$(grep \| < results/resultsphpcs | grep -c WARNING)
Print_errors "$phpcsErr" "PHPCS"
Print_warning "$phpcsWarn" "PHPCS"
NotePHPCS "$phpcsErr" "$phpcsWarn"


Finale_Note $FinalNote