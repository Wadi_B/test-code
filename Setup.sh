#!/bin/bash


#check si sudo
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi
mkdir /home/Codecoverage
cd /home/Codecoverage

echo "#------------------------------------------------ Basic Operations ------------------------------------------------#"

apt -y update
apt -y upgrade
apt -y install curl
apt -y install wget
apt -y install software-properties-common
echo Europe/Paris >> /etc/timezone
apt -y install unzip
# apt -y install python-software-properties

echo "#------------------------------------------------ Install GIT ------------------------------------------------#"

apt -y install git

echo "#------------------------------------------------ Install PHP ------------------------------------------------#"

# apt -y install apache2
# add-apt-repository -y ppa:ondrej/php
# apt -y update
apt -y install php7.3-cli php7.3-common php7.3-opcache php7.3-curl php7.3-mbstring php7.3-mysql php7.3-zip php7.3-xml

echo "#------------------------------------------------ Install COMPOSER ------------------------------------------------#"

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

echo "#------------------------------------------------ Install shellcheck ------------------------------------------------#"

apt -y install shellcheck

echo "#------------------------------------------------ Install PHPLINT ------------------------------------------------#"

composer require overtrue/phplint --dev -vvv

echo "#------------------------------------------------ Install PHPCPD ------------------------------------------------#"

wget https://phar.phpunit.de/phpcpd.phar
chmod +x phpcpd.phar
mv phpcpd.phar /usr/local/bin/phpcpd

composer require --dev sebastian/phpcpd

echo "#------------------------------------------------ Install PHP CODESNIFFER ------------------------------------------------#"

curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar

composer global require "squizlabs/php_codesniffer=*"

mv phpcs.phar /usr/local/bin/phpcs
chmod 775 /usr/local/bin/phpcs
# chmod 775 /usr/bin/phpcs


# mv phpcbf.phar /usr/local/bin/phpcbf
# chmod 775 /usr/bin/phpcbf
# chmod 775 /usr/local/bin/phpcbf



# VZ8cAQPbcPzQDH-zWLpA
# docker-compose up -d --build